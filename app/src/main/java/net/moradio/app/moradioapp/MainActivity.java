package net.moradio.app.moradioapp;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.TimedText;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.IOException;


public class MainActivity extends Activity {


    private Button buttonStartRadio;
    private Button buttonStopRadio;

    private MediaPlayer player;

    private RadioGroup radioGroup;
    private RadioButton radioButtonMoradio;
    private RadioButton radioButtonTest;

    private String radioURI;
    private final String testURI = "http://usa8-vn.mixstream.net:8138";
    // TODO: insert the REAL moradio URL
    private final String moradioURI = "http://stream.tunestew.com:8800";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initGUI();
        radioURI = moradioURI;
    }


    private void initGUI() {

        buttonStartRadio = (Button) findViewById(R.id.buttonStart);

        buttonStopRadio = (Button) findViewById(R.id.buttonStop);
        buttonStopRadio.setEnabled(false);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioButtonMoradio = (RadioButton) findViewById(R.id.radioButtonMoradio);
        radioButtonTest = (RadioButton) findViewById(R.id.radioButtonTest);
        radioButtonMoradio.setChecked(true);
    }


    public void startRadio(View view) {
        doStartRadio();
    }

    private void doStartRadio(){
        System.out.println("Starting radio...");
        updateRadioURI();
        player = new MediaPlayer();
        try {
            player.setDataSource(radioURI);
        } catch (IOException e) {
            e.printStackTrace();
        }
        buttonStopRadio.setEnabled(true);
        buttonStartRadio.setEnabled(false);

        player.prepareAsync();

        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mp) {
                player.start();
                printMediaPlayerInfo(mp);
            }
        });

        player.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                System.err.println("On Error! what = " + what + " extra = " + extra);
                doStopRadio();
                return false;
            }
        });

        player.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            @Override
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                printMediaPlayerInfo(mp);
                return false;
            }
        });

        player.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            @Override
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                // TODO: is always 0. Test this with the actual moradio stream.
                //System.out.println("Buffering = "+percent+"%");
            }
        });

        player.setOnTimedTextListener(new MediaPlayer.OnTimedTextListener() {
            @Override
            public void onTimedText(MediaPlayer mp, TimedText text) {
                System.out.println("Timed text = "+text.getText());
            }
        });
    }

    private void printMediaPlayerInfo(MediaPlayer mp){
        if(mp != null && mp.isLooping()){
            System.out.println("CurrentPosition = "+mp.getCurrentPosition());
            System.out.println("Duration = "+mp.getDuration());
            System.out.println("trackInfo = "+mp.getTrackInfo()[0]);
        }
    }

    public void stopRadio(View view) {
        doStopRadio();
    }

    private void doStopRadio(){
        System.out.println("Stopping radio...");

        buttonStartRadio.setEnabled(true);
        buttonStopRadio.setEnabled(false);

        if (player.isPlaying()) {
            player.stop();
            player.release();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Should we pause the player here?
    }

    private void updateRadioURI() {
        System.out.println("Updating radio uri...");

        int selectedRadioButton = radioGroup.getCheckedRadioButtonId();
        if (selectedRadioButton == (radioButtonMoradio.getId())) {
            this.radioURI = moradioURI;
        } else if (selectedRadioButton == (radioButtonTest.getId())) {
            this.radioURI = testURI;
        }
        else{
            System.err.println("Unknown radio button selected!");
        }
        System.out.println("New URI = "+this.radioURI);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
